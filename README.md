# variational-siamese-network

## Overview

Tensorflow implementation of [Learning semantic similarity in a continuous space](https://papers.nips.cc/paper_files/paper/2018/hash/97e8527feaf77a97fc38f34216141515-Abstract.html)

<img align="center" img src="./img/Gauss.png">

## Requirements

- [Python 3.5+](https://anaconda.org/anaconda/python)
- [TensorFlow 1.3.0+](https://www.tensorflow.org/install/)
- [Tqdm](https://pypi.python.org/pypi/tqdm)

## Usage

Pretrained word-2-vec language model, under w2v, from [GloVe](https://nlp.stanford.edu/projects/glove/) or [Word2Vec](https://github.com/mmihaltz/word2vec-GoogleNews-vectors).

Semantic Similarity Dataset from quora_duplicate_questions.tsv for train/dev/test, saved under data/split folder as text files.

- To train a variational siamese network with generative pretraining (repeat), run the following snipped in the folder _deepNLU:
```
> python train.py VAE
```
- For the repeat, reformulate framework, run:
```
> python train.py VAD
```

- To visualize training on tensorboard, run:
```
> tensorboard --logdir=summary
```

- To test a trained model / for inference, run eval.ipynb

## Architecture and Training

### Variational Auto Encoder (VAE)

<img align="center" img src="./img/Repeat, Reformulate.png">

### Variational Siamese Network

<img align="center" img src="./img/Variational_Siamese.png">

##  Semantic Similarity Evaluation

<img align="center" img src="./img/tensorboard2.png">

<img align="center" img src="./img/density.png">

## Acknowledgments

[Ecole Polytechnique](http://www.polytechnique.edu/),Télécom Paris-Tech

[Pr. Chloé Clavel](https://clavel.wp.imt.fr/), [Pr. Gabriel Peyré](http://www.gpeyre.com/), [Pr. Francis Bach](https://www.di.ens.fr/~fbach/), [Pr. Guillaume Obozinski](http://imagine.enpc.fr/~obozinsg/), [Pr. Michalis Vazirgiannis](http://www.lix.polytechnique.fr/Labo/Michalis.Vazirgiannis/)

Magdalena Fuentes, Constance Nozière and Paul Bertin

Reviewers for their valuable comments and feedback
